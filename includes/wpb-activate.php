<?php 

    function custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Books', 'Post Type General Name', 'twentytwenty' ),
            'singular_name'       => _x( 'Book', 'Post Type Singular Name', 'twentytwenty' ),
            'menu_name'           => __( 'Books', 'twentytwenty' ),
            'parent_item_colon'   => __( 'Parent Book', 'twentytwenty' ),
            'all_items'           => __( 'All Books', 'twentytwenty' ),
            'view_item'           => __( 'View Book', 'twentytwenty' ),
            'add_new_item'        => __( 'Add New Book', 'twentytwenty' ),
            'add_new'             => __( 'Add New', 'twentytwenty' ),
            'edit_item'           => __( 'Edit Book', 'twentytwenty' ),
            'update_item'         => __( 'Update Book', 'twentytwenty' ),
            'search_items'        => __( 'Search Book', 'twentytwenty' ),
            'not_found'           => __( 'Not Found', 'twentytwenty' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Books', 'twentytwenty' ),
            'description'         => __( 'Book news and reviews', 'twentytwenty' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'taxonomies'          => array( 'genres' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
     
        );
         
        // Registering your Custom Post Type
        register_post_type( 'Books', $args );

        global $table_prefix, $wpdb;

        $tblname = 'booksmeta';
        $wp_track_table = $table_prefix . "$tblname";

        #Check to see if the table exists already, if not, then create it

        if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
        {

        $sql = "CREATE TABLE `". $wp_track_table ."` ( ";
        $sql .= "  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT, ";
        $sql .= "  `books_id` bigint(20) NOT NULL DEFAULT '0', ";
        $sql .= "  `meta_key` varchar(255) DEFAULT NULL, ";
        $sql .= "  `meta_value` longtext, PRIMARY KEY (`meta_id`), ";
        $sql .= "  KEY `book_id` (`book_id`), KEY `meta_key` (`meta_key`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
        
        }
     
    }

    function wpb_register_metadata_table() {
      global $table_prefix, $wpdb;
      $wpdb->booksmeta = $wpdb->prefix . 'booksmeta';
    }
    

    function create_book_hierarchical_taxonomy() {
 
        // Add new taxonomy, make it hierarchical like categories
        //first do the translations part for GUI
         
          $labels = array(
            'name' => _x( 'Book Category', 'taxonomy general name' ),
            'singular_name' => _x( 'Book Category', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Book Category' ),
            'all_items' => __( 'All Book Categories' ),
            'parent_item' => __( 'Parent Book Category' ),
            'parent_item_colon' => __( 'Parent Book Category:' ),
            'edit_item' => __( 'Edit Book Category' ), 
            'update_item' => __( 'Update Book Category' ),
            'add_new_item' => __( 'Add New Book Category' ),
            'new_item_name' => __( 'New Book Category Name' ),
            'menu_name' => __( 'Book Categories' ),
          );    
         
        // Now register the taxonomy
          register_taxonomy('book_category',array('books'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'book_category' ),
          ));
         
    }

    function create_book_nonhierarchical_taxonomy() {
 
        // Labels part for the GUI
         
          $labels = array(
            'name' => _x( 'Book Tags', 'taxonomy general name' ),
            'singular_name' => _x( 'Book Tag', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Book Tags' ),
            'popular_items' => __( 'Popular Book Tags' ),
            'all_items' => __( 'All Book Tags' ),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __( 'Edit Book Tag' ), 
            'update_item' => __( 'Update Book Tag' ),
            'add_new_item' => __( 'Add New Book Tag' ),
            'new_item_name' => __( 'New Book Tag Name' ),
            'separate_items_with_commas' => __( 'Separate Book Tags with commas' ),
            'add_or_remove_items' => __( 'Add or remove Book Tags' ),
            'choose_from_most_used' => __( 'Choose from the most used Book Tags' ),
            'menu_name' => __( 'Book Tags' ),
          ); 
         
        // Now register the non-hierarchical taxonomy like tag
         
          register_taxonomy('book_tags','books',array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array( 'slug' => 'book_tag' ),
          ));
        }


?>