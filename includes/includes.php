<?php

    include('wpb-activate.php');
    include('wpb-deactivate.php');
    include('admin/settings.php');
    include('shortcode/shortcode.php');
    include('widget/wpb_widget.php');

    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not
    * unnecessarily executed.
    */

    add_action( 'init', 'custom_post_type', 0 );
    add_action( 'init', 'wpb_register_metadata_table' );
    add_action( 'init', 'create_book_hierarchical_taxonomy', 0 );
    add_action( 'init', 'create_book_nonhierarchical_taxonomy', 0 );

    add_shortcode('book', 'wpb_book_shortcode');


?>