<?php 

// Creating the widget 
class wpb_widget extends WP_Widget {
  
    function __construct() {
        parent::__construct(
            
            // Base ID of your widget
            'wpb_widget', 
            
            // Widget name will appear in UI
            __('WP Books Widget', 'wpb_widget_domain'), 
            
            // Widget description
            array( 'description' => __( 'Books Widget', 'wpb_widget_domain' ), ) 
        );
    }
    
    // Creating widget front-end
    
    public function widget( $args, $instance ) {
        $category = apply_filters( 'widget_category', $instance['category'] );
    
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $category ) )
        echo 'Category = '. $category.'<br>';

        $args = array(
            'post_type' => 'books',
            'tax_query' => array(
                array(
                    'taxonomy' => 'book_category',
                    'field' => 'slug',
                    'terms' => array( $category ),
                )
            )
        );
        $html = '';
        $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) :
                    //echo "<br>Sql  = ";
                    //echo $GLOBALS['wp_query']->request;
                    while ( $the_query->have_posts() ) : $the_query->the_post();

                        $id = get_the_ID();
                        $html .= get_the_title().'<br>';
                    // Do Stuff
                    endwhile;
                    endif;
        echo $html;
        
        // This is where you run the code and display the output
        //echo $args['after_widget'];
    }
            
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'category' ] ) ) {
        $category = $instance[ 'category' ];
        }
        else {
        $category = __( 'Category', 'wpb_widget_domain' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'category:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'category' ); ?>" name="<?php echo $this->get_field_name( 'category' ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>" />
        </p>
    <?php 
    }
        
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['category'] = ( ! empty( $new_instance['category'] ) ) ? strip_tags( $new_instance['category'] ) : '';
        return $instance;
    }
 
// Class wpb_widget ends here
} 
 
 
// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

?>