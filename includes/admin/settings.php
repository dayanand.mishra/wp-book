<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */
 
/**
 * custom option and settings
 */
function wpb_settings_init() {
    // Register a new setting for "wpb" page.
    register_setting( 'wpb', 'wpb_options' );
 
    // Register a new section in the "wpb" page.
    add_settings_section(
        'wpb_section_developers',
        __( 'Books Settings.', 'wpb' ), 'wpb_section_developers_callback',
        'wpb'
    );
 
    // Register a new field in the "wpb_section_developers" section, inside the "wpb" page.
    add_settings_field(
        'wpb_field_currency', // As of WP 4.6 this value is used only internally.
                                // Use $args' label_for to populate the id inside the callback.
            __( 'Currency', 'wpb' ),
        'wpb_field_currency_cb',
        'wpb',
        'wpb_section_developers',
        array(
            'label_for'         => 'wpb_field_currency',
            'class'             => 'wpb_row',
            'wpb_custom_data' => 'custom',
        )
    );

    add_settings_field(
        'wpb_field_book_count', // As of WP 4.6 this value is used only internally.
                                // Use $args' label_for to populate the id inside the callback.
            __( 'Book count per page', 'wpb' ),
        'wpb_field_book_count_cb',
        'wpb',
        'wpb_section_developers',
        array(
            'label_for'         => 'wpb_field_book_count',
            'class'             => 'wpb_row',
            'wpb_custom_data' => 'custom',
        )
    );
}
 
/**
 * Register our wpb_settings_init to the admin_init action hook.
 */
add_action( 'admin_init', 'wpb_settings_init' );
 
 
/**
 * Custom option and settings:
 *  - callback functions
 */
 
 
/**
 * Developers section callback function.
 *
 * @param array $args  The settings array, defining title, id, callback.
 */
function wpb_section_developers_callback( $args ) {
    ?>
    <p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( '', 'wpb' ); ?></p>
    <?php
}
 
/**
 * currency field callbakc function.
 *
 * WordPress has magic interaction with the following keys: label_for, class.
 * - the "label_for" key value is used for the "for" attribute of the <label>.
 * - the "class" key value is used for the "class" attribute of the <tr> containing the field.
 * Note: you can add custom key value pairs to be used inside your callbacks.
 *
 * @param array $args
 */
function wpb_field_currency_cb( $args ) {
    // Get the value of the setting we've registered with register_setting()
    $options = get_option( 'wpb_options' );
    ?>
    <select
            id="<?php echo esc_attr( $args['label_for'] ); ?>"
            data-custom="<?php echo esc_attr( $args['wpb_custom_data'] ); ?>"
            name="wpb_options[<?php echo esc_attr( $args['label_for'] ); ?>]">
        <option value="INR" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'INR', false ) ) : ( '' ); ?>>
            <?php esc_html_e( 'Indian Rupees', 'wpb' ); ?>
        </option>
        <option value="USD" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'USD', false ) ) : ( '' ); ?>>
            <?php esc_html_e( 'US Dollars', 'wpb' ); ?>
        </option>
    </select>
    
    <?php
}

function wpb_field_book_count_cb( $args ){
    $options = get_option( 'wpb_options' );
    ?>
    <input type="number" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="wpb_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo esc_attr( $options[ $args['label_for'] ] ); ?>">
    <?php
}
 
/**
 * Add the top level menu page.
 */
function wpb_options_page() {
    add_menu_page(
        'Booksmenu',
        'Booksmenu',
        'manage_options',
        'wpb',
        'wpb_options_page_html'
    );
}
 
 
/**
 * Register our wpb_options_page to the admin_menu action hook.
 */
add_action( 'admin_menu', 'wpb_options_page' );
 
 
/**
 * Top level menu callback function
 */
function wpb_options_page_html() {
    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
 
    // add error/update messages
 
    // check if the user have submitted the settings
    // WordPress will add the "settings-updated" $_GET parameter to the url
    if ( isset( $_GET['settings-updated'] ) ) {
        // add settings saved message with the class of "updated"
        add_settings_error( 'wpb_messages', 'wpb_message', __( 'Settings Saved', 'wpb' ), 'updated' );
    }
 
    // show error/update messages
    settings_errors( 'wpb_messages' );
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
        <form action="options.php" method="post">
            <?php
            // output security fields for the registered setting "wpb"
            settings_fields( 'wpb' );
            // output setting sections and their fields
            // (sections are registered for "wpb", each field is registered to a specific section)
            do_settings_sections( 'wpb' );
            // output save settings button
            submit_button( 'Save Settings' );
            ?>
        </form>
    </div>
    <?php
}