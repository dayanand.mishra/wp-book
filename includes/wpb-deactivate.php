<?php 
    function wpb_uninstall() {
    // Uninstallation stuff here
        unregister_post_type( 'Books' );
        unregister_taxonomy( 'book_category');
        unregister_taxonomy( 'book_tags');

        global $table_prefix, $wpdb;
        $tblname = 'booksmeta';

        $wp_track_table = $table_prefix . "$tblname";

        $sql = "DROP TABLE IF EXISTS `".$wp_track_table."`";
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
        //echo $sql;

    }

?>