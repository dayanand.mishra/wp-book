<?php
/**
 * Plugin Name: WP Book
 * Plugin URI: https://pippinsplugins.com/how-to-begin-writing-your-first-wordpress-plugin
 * Description: This is my first WordPress Plugin
 * Author: Dayanand Mishra
 * Author URI: https://pippinsplugins.com
 * Version: 1.0
 */

    include('includes/includes.php');
    /* Fire our meta box setup function on the post editor screen. */
    // add_action( 'load-post.php', 'books_meta_boxes_setup' );
    // add_action( 'load-post-new.php', 'books_meta_boxes_setup' );

     /* Add meta boxes on the 'add_meta_boxes' hook. */
    add_action( 'add_meta_boxes', 'books_add_post_meta_boxes' );

    /* Save post meta on the 'save_post' hook. */
    add_action( 'save_post', 'books_save_post_author_meta', 10, 2 );


/* Create one or more meta boxes to be displayed on the post editor screen. */
function books_add_post_meta_boxes() {

    add_meta_box(
        'books-post-author',      // Unique ID
        esc_html__( 'Book Details', 'John Doe' ),    // Title
        'books_author_meta_box',   // Callback function
        'Books',         // Admin page (or post type)
        'advanced',         // Context
        'default'         // Priority
    );

}

    /* Display the post meta box. */
    function books_author_meta_box( $post ) {
        $bopt = get_option( 'wpb_options' );
    ?>

    <?php wp_nonce_field( basename( __FILE__ ), 'books_post_author_nonce' ); ?>

        <p>
        <label for="books-author-name"><?php _e( "Add book author name.", 'John Doe' ); ?></label>
        <br />
        <input class="widefat" type="text" name="books-author-name" id="books-author-name" value="<?php echo esc_attr( get_metadata( 'books', $post->ID, 'books-author-name', $single = true ) ); ?>" size="30" />
        </p>

        <p>
        <label for="books-price"><?php _e( "Add book price. (in ".$bopt['wpb_field_currency'].")", '500' ); ?></label>
        <br />
        <input class="widefat" type="text" name="books-price" id="books-price" value="<?php echo esc_attr( get_metadata( 'books', $post->ID, 'books-price', $single = true ) ); ?>" size="30" />
        </p>

        <p>
        <label for="books-publisher"><?php _e( "Add book publisher.", 'Test' ); ?></label>
        <br />
        <input class="widefat" type="text" name="books-publisher" id="books-publisher" value="<?php echo esc_attr( get_metadata( 'books', $post->ID, 'books-publisher', $single = true ) ); ?>" size="30" />
        </p>

        <p>
        <label for="books-year"><?php _e( "Add book year.", '2015' ); ?></label>
        <br />
        <input class="widefat" type="text" name="books-year" id="books-year" value="<?php echo esc_attr( get_metadata( 'books', $post->ID, 'books-year', $single = true ) ); ?>" size="30" />
        </p>

        <p>
        <label for="books-edition"><?php _e( "Add book edition.", 'One' ); ?></label>
        <br />
        <input class="widefat" type="text" name="books-edition" id="books-edition" value="<?php echo esc_attr( get_metadata( 'books', $post->ID, 'books-edition', $single = true ) ); ?>" size="30" />
        </p>

        <p>
        <label for="books-url"><?php _e( "Add book url.", 'One' ); ?></label>
        <br />
        <input class="widefat" type="text" name="books-url" id="books-url" value="<?php echo esc_attr( get_metadata( 'books', $post->ID, 'books-url', $single = true ) ); ?>" size="30" />
        </p>

    <?php }

    function books_save_post_author_meta($post_id, $post){
        /* Verify the nonce before proceeding. */
        if ( !isset( $_POST['books_post_author_nonce'] ) || !wp_verify_nonce( $_POST['books_post_author_nonce'], basename( __FILE__ ) ) )
        return $post_id;

        /* Get the post type object. */
        $post_type = get_post_type_object( $post->post_type );

        /* Check if the current user has permission to edit the post. */
        if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
        return $post_id;

        if(isset($_POST['books-author-name']))
        {
            $new_meta_value = sanitize_text_field( $_POST['books-author-name'] );

            $meta_key = 'books-author-name';

            /* Get the meta value of the custom field key. */
            $meta_value = get_metadata( 'books', $post_id, $meta_key, $single = true );


            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && ’ == $meta_value ){
            $added = add_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If the new meta value does not match the old value, update it. */
            } elseif ( $new_meta_value && $new_meta_value != $meta_value ){
            $updated = update_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            } elseif ( ’ == $new_meta_value && $meta_value ) {
            $deleted = delete_metadata( 'books', $post_id, $meta_key );
            }
        }

        if(isset($_POST['books-price']))
        {
            $new_meta_value = sanitize_text_field( $_POST['books-price'] );

            $meta_key = 'books-price';

            /* Get the meta value of the custom field key. */
            $meta_value = get_metadata( 'books', $post_id, $meta_key, $single = true );


            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && ’ == $meta_value ){
            $added = add_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If the new meta value does not match the old value, update it. */
            } elseif ( $new_meta_value && $new_meta_value != $meta_value ){
            $updated = update_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            } elseif ( ’ == $new_meta_value && $meta_value ) {
            $deleted = delete_metadata( 'books', $post_id, $meta_key );
            }
        }

        if(isset($_POST['books-publisher']))
        {
            $new_meta_value = sanitize_text_field( $_POST['books-publisher'] );

            $meta_key = 'books-publisher';

            /* Get the meta value of the custom field key. */
            $meta_value = get_metadata( 'books', $post_id, $meta_key, $single = true );


            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && ’ == $meta_value ){
            $added = add_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If the new meta value does not match the old value, update it. */
            } elseif ( $new_meta_value && $new_meta_value != $meta_value ){
            $updated = update_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            } elseif ( ’ == $new_meta_value && $meta_value ) {
            $deleted = delete_metadata( 'books', $post_id, $meta_key );
            }
        }

        if(isset($_POST['books-year']))
        {
            $new_meta_value = sanitize_text_field( $_POST['books-year'] );

            $meta_key = 'books-year';

            /* Get the meta value of the custom field key. */
            $meta_value = get_metadata( 'books', $post_id, $meta_key, $single = true );


            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && ’ == $meta_value ){
            $added = add_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If the new meta value does not match the old value, update it. */
            } elseif ( $new_meta_value && $new_meta_value != $meta_value ){
            $updated = update_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            } elseif ( ’ == $new_meta_value && $meta_value ) {
            $deleted = delete_metadata( 'books', $post_id, $meta_key );
            }
        }

        if(isset($_POST['books-edition']))
        {
            $new_meta_value = sanitize_text_field( $_POST['books-edition'] );

            $meta_key = 'books-edition';

            /* Get the meta value of the custom field key. */
            $meta_value = get_metadata( 'books', $post_id, $meta_key, $single = true );


            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && ’ == $meta_value ){
            $added = add_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If the new meta value does not match the old value, update it. */
            } elseif ( $new_meta_value && $new_meta_value != $meta_value ){
            $updated = update_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            } elseif ( ’ == $new_meta_value && $meta_value ) {
            $deleted = delete_metadata( 'books', $post_id, $meta_key );
            }
        }

        if(isset($_POST['books-url']))
        {
            $new_meta_value = sanitize_text_field( $_POST['books-url'] );

            $meta_key = 'books-url';

            /* Get the meta value of the custom field key. */
            $meta_value = get_metadata( 'books', $post_id, $meta_key, $single = true );


            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && ’ == $meta_value ){
            $added = add_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If the new meta value does not match the old value, update it. */
            } elseif ( $new_meta_value && $new_meta_value != $meta_value ){
            $updated = update_metadata( 'books', $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            } elseif ( ’ == $new_meta_value && $meta_value ) {
            $deleted = delete_metadata( 'books', $post_id, $meta_key );
            }
        }

    }





    function wpb_flush_rewrites() {
        custom_post_type();
        flush_rewrite_rules();
    }

    register_activation_hook( __FILE__, 'wpb_flush_rewrites' );

    register_uninstall_hook( __FILE__, 'wpb_uninstall' );

    add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

    function my_custom_dashboard_widgets() {
    global $wp_meta_boxes;

    wp_add_dashboard_widget('custom_help_widget', 'Top 5 Book Categories', 'custom_dashboard_help');
    }

    function custom_dashboard_help() {
        $args = array(
            'p' => 37,
            'post_type' => 'books',
            'taxonomy' => 'book_category',
            'orderby' => 'count',
            'order' => 'DESC',
            'number' => '5'

        );
        $thtml = '<ul>';
        $query = get_terms( $args );
        foreach($query as $q)
        {
            $thtml .= '<li><div><span>Book Category: </span><span>'.$q->name.'</span></div><div><span>Count: </span><span>'.$q->count.'</span></div></li>';
        }
        $thtml .= '</ul>';
    echo $thtml;
    }


?>